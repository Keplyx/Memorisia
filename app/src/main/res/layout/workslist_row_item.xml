<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (c) 2018.
  ~ This file is part of Memorisia.
  ~
  ~ Memorisia is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ Memorisia is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU General Public License for more details.
  ~
  ~  You should have received a copy of the GNU General Public License
  ~  along with Memorisia.  If not, see <http://www.gnu.org/licenses/>.
  -->

<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="@dimen/worklist_row_active"
    android:animateLayoutChanges="true"
    android:clickable="true"
    android:focusable="true"
    android:foreground="?android:attr/selectableItemBackground"
    android:orientation="vertical">

    <android.support.v7.widget.CardView
        android:id="@+id/cardContainer"
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_marginBottom="8dp"
        android:layout_marginEnd="8dp"
        android:layout_marginLeft="8dp"
        android:layout_marginRight="8dp"
        android:layout_marginStart="8dp"
        android:layout_marginTop="8dp"
        app:cardBackgroundColor="?attr/colorCardBackground"
        app:cardCornerRadius="2dp"
        app:cardElevation="4dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent">

        <android.support.constraint.ConstraintLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:animateLayoutChanges="true">

            <android.support.v7.widget.CardView
                android:id="@+id/titleCard"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                app:cardBackgroundColor="?attr/colorCardTitleBackground"
                app:cardCornerRadius="2dp"
                app:cardElevation="2dp"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent">

                <android.support.constraint.ConstraintLayout
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:animateLayoutChanges="true">

                    <ImageView
                        android:id="@+id/mainLogo"
                        android:layout_width="12dp"
                        android:layout_height="12dp"
                        android:layout_marginBottom="8dp"
                        android:layout_marginLeft="8dp"
                        android:layout_marginStart="8dp"
                        android:layout_marginTop="8dp"
                        app:layout_constraintBottom_toBottomOf="parent"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toTopOf="parent"
                        app:srcCompat="@mipmap/ic_launcher" />

                    <TextView
                        android:id="@+id/workTitle"
                        android:layout_width="0dp"
                        android:layout_height="@dimen/worktitle_active"
                        android:layout_marginBottom="2dp"
                        android:layout_marginEnd="8dp"
                        android:layout_marginLeft="8dp"
                        android:layout_marginRight="8dp"
                        android:layout_marginStart="8dp"
                        android:layout_marginTop="2dp"
                        android:ellipsize="end"
                        android:forceHasOverlappingRendering="false"
                        android:gravity="center_vertical"
                        android:maxLines="1"
                        android:text="Title"
                        android:textAppearance="@style/TextAppearance.AppCompat"
                        android:textSize="14sp"
                        android:textStyle="bold"
                        app:layout_constraintBottom_toBottomOf="parent"
                        app:layout_constraintEnd_toStartOf="@+id/priorityRatingBar"
                        app:layout_constraintStart_toEndOf="@+id/mainLogo"
                        app:layout_constraintTop_toTopOf="parent"
                        tools:layout_constraintTop_creator="1" />

                    <RatingBar
                        android:id="@+id/priorityRatingBar"
                        style="@style/Widget.AppCompat.RatingBar.Small"
                        android:layout_width="wrap_content"
                        android:layout_height="16dp"
                        android:layout_marginEnd="8dp"
                        android:layout_marginRight="8dp"
                        android:layout_marginTop="4dp"
                        android:numStars="5"
                        android:stepSize="1"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintTop_toTopOf="parent" />

                    <ImageButton
                        android:id="@+id/editButton"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:tint="?android:attr/textColorPrimary"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintTop_toBottomOf="@+id/priorityRatingBar"
                        app:srcCompat="@drawable/ic_edit_black_24dp" />
                </android.support.constraint.ConstraintLayout>
            </android.support.v7.widget.CardView>

            <TextView
                android:id="@+id/SubjectTextView"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginLeft="8dp"
                android:layout_marginTop="2dp"
                android:text="Subject"
                android:textAppearance="@style/TextAppearance.AppCompat.Small"
                android:textSize="12sp"
                app:layout_constraintLeft_toRightOf="@+id/subjectLogo"
                app:layout_constraintTop_toBottomOf="@+id/workTypeTextView"
                tools:layout_constraintLeft_creator="1"
                tools:layout_constraintTop_creator="1" />

            <ImageView
                android:id="@+id/subjectLogo"
                android:layout_width="12dp"
                android:layout_height="12dp"
                android:layout_marginLeft="8dp"
                app:layout_constraintBottom_toBottomOf="@+id/SubjectTextView"
                app:layout_constraintLeft_toLeftOf="parent"
                app:layout_constraintTop_toTopOf="@+id/SubjectTextView"
                app:srcCompat="@mipmap/ic_launcher"
                tools:layout_constraintLeft_creator="1"
                tools:layout_constraintTop_creator="1" />

            <CheckBox
                android:id="@+id/stateCheckBox"
                style="@style/Widget.AppCompat.CompoundButton.CheckBox"
                android:layout_width="48dp"
                android:layout_height="48dp"
                android:background="?android:attr/listChoiceIndicatorMultiple"
                android:button="@null"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/titleCard" />

            <ImageView
                android:id="@+id/agendaLogo"
                android:layout_width="12dp"
                android:layout_height="12dp"
                android:layout_marginLeft="8dp"
                app:layout_constraintBottom_toBottomOf="@+id/AgendaTextView"
                app:layout_constraintLeft_toLeftOf="parent"
                app:layout_constraintTop_toTopOf="@+id/AgendaTextView"
                app:srcCompat="@mipmap/ic_launcher"
                tools:layout_constraintTop_creator="1" />

            <TextView
                android:id="@+id/AgendaTextView"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginLeft="8dp"
                android:layout_marginTop="2dp"
                android:text="Agenda"
                android:textAppearance="@style/TextAppearance.AppCompat.Small"
                android:textSize="12sp"
                app:layout_constraintLeft_toRightOf="@+id/agendaLogo"
                app:layout_constraintTop_toBottomOf="@+id/SubjectTextView"
                tools:layout_constraintLeft_creator="1"
                tools:layout_constraintTop_creator="1" />

            <ImageView
                android:id="@+id/workLogo"
                android:layout_width="12dp"
                android:layout_height="12dp"
                android:layout_marginLeft="8dp"
                android:layout_marginStart="8dp"
                app:layout_constraintBottom_toBottomOf="@+id/workTypeTextView"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="@+id/workTypeTextView"
                app:srcCompat="@mipmap/ic_launcher"
                tools:layout_constraintLeft_creator="1"
                tools:layout_constraintTop_creator="1" />

            <TextView
                android:id="@+id/dateTextView"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginEnd="4dp"
                android:layout_marginRight="4dp"
                android:layout_marginTop="4dp"
                android:gravity="right|center_vertical"
                android:text="99/99/9999"
                android:textAppearance="@style/TextAppearance.AppCompat.Small"
                android:textSize="10sp"
                app:layout_constraintEnd_toStartOf="@+id/dateImage"
                app:layout_constraintRight_toRightOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/titleCard" />

            <TextView
                android:id="@+id/timeTextView"
                android:layout_width="54dp"
                android:layout_height="wrap_content"
                android:gravity="right|center_vertical"
                android:text="99:99"
                android:textAppearance="@style/TextAppearance.AppCompat.Small"
                android:textSize="10sp"
                app:layout_constraintEnd_toEndOf="@+id/dateTextView"
                app:layout_constraintRight_toRightOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/dateTextView"
                tools:layout_constraintRight_creator="1" />

            <TextView
                android:id="@+id/workTypeTextView"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginLeft="8dp"
                android:layout_marginTop="4dp"
                android:text="Type"
                android:textAppearance="@style/TextAppearance.AppCompat.Small"
                android:textSize="12sp"
                app:layout_constraintLeft_toRightOf="@+id/workLogo"
                app:layout_constraintTop_toBottomOf="@+id/titleCard"
                tools:layout_constraintLeft_creator="1"
                tools:layout_constraintTop_creator="1" />

            <ImageView
                android:id="@+id/dateImage"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginEnd="8dp"
                android:layout_marginRight="8dp"
                app:layout_constraintBottom_toBottomOf="@+id/timeTextView"
                app:layout_constraintEnd_toStartOf="@+id/stateCheckBox"
                app:layout_constraintTop_toTopOf="@+id/dateTextView"
                app:srcCompat="@drawable/ic_date_range_black_24dp" />

        </android.support.constraint.ConstraintLayout>
    </android.support.v7.widget.CardView>

</android.support.constraint.ConstraintLayout>