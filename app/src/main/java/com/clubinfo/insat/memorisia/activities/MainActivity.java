/*
 * Copyright (c) 2018.
 * This file is part of Memorisia.
 *
 * Memorisia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Memorisia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Memorisia.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.clubinfo.insat.memorisia.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.clubinfo.insat.memorisia.R;
import com.clubinfo.insat.memorisia.database.MemorisiaDatabase;
import com.clubinfo.insat.memorisia.fragments.AllWorksFragment;
import com.clubinfo.insat.memorisia.fragments.BaseFragment;
import com.clubinfo.insat.memorisia.fragments.CalendarFragment;
import com.clubinfo.insat.memorisia.fragments.HomeFragment;
import com.clubinfo.insat.memorisia.fragments.SubjectsFragment;
import com.clubinfo.insat.memorisia.fragments.WorkTypesFragment;
import com.clubinfo.insat.memorisia.fragments.WorkViewFragment;
import com.clubinfo.insat.memorisia.modules.OptionModule;
import com.clubinfo.insat.memorisia.modules.WorkModule;
import com.clubinfo.insat.memorisia.utils.ModulesUtils;
import com.clubinfo.insat.memorisia.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static String PACKAGE_NAME;
    Menu menu;
    MenuItem editButton;
    MenuItem sortButton;
    MenuItem agendaButton;
    private DrawerLayout drawer;
    private FloatingActionButton fab;
    private Context context;
    private boolean isNightMode;
    private List<Integer> selectedAgendas = new ArrayList<>();
    private Frags activeFragment = Frags.FRAG_HOME;
    private Frags backStackFragment = null;

    public Frags getActiveFragment() {
        return activeFragment;
    }

    public void setActiveFragment(Frags activeFragment) {
        this.activeFragment = activeFragment;
    }

    public Frags getBackStackFragment() {
        return backStackFragment;
    }

    public void setBackStackFragment(Frags backStackFragment) {
        this.backStackFragment = backStackFragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setNightMode(this, false);
        setContentView(R.layout.activity_main);


        isNightMode = Utils.isNightMode(this);

        context = this;
        PACKAGE_NAME = getPackageName();
        selectedAgendas = Utils.getSelectedAgendasFromPrefs(context);

        if (savedInstanceState == null)
            getFragmentManager().beginTransaction().replace(R.id.content_frame, new HomeFragment(), Frags.FRAG_HOME.name()).commit();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        initNavigationView();
        initFabButton();

        Utils.checkModulesPresent(this);
        Utils.checkWorkOutdated(this);
    }

    public void initNavigationView() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_home);
    }

    public void initFabButton() {
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewWork();
            }
        });
    }

    public FloatingActionButton getFabButton() {
        return fab;
    }

    /**
     * Creates a new {@link com.clubinfo.insat.memorisia.activities.EditWorkActivity EditWorkActivity}
     * with default values.
     */
    private void createNewWork() {
        Intent intent = new Intent(context, EditWorkActivity.class);
        WorkModule work = new WorkModule(-1, -1, -1, 0, new int[]{-1, -1, -1}, new int[]{-1, -1}, "", false);
        work.setId(-1);
        if (isFragmentActive(Frags.FRAG_WORKS)) {
            WorkViewFragment workFragment = (WorkViewFragment) getFragmentManager().findFragmentByTag(Frags.FRAG_WORKS.name());
            if (workFragment.getParentType() == OptionModule.SUBJECT)
                work.setSubjectId(workFragment.getParentId());

            if (workFragment.getParentType() == OptionModule.WORK_TYPE)
                work.setWorkTypeId(workFragment.getParentId());

        } else if (isFragmentActive(Frags.FRAG_CALENDAR)) {
            CalendarFragment calendarFragment = (CalendarFragment) getFragmentManager().findFragmentByTag(Frags.FRAG_CALENDAR.name());
            work.setDate(calendarFragment.getSelectedDate());
        }
        List<Integer> agendas = getSelectedAgendas();
        if (agendas.size() == 1)
            work.setAgendaId(agendas.get(0));

        intent.putExtras(ModulesUtils.createBundleFromModule(work));
        context.startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean nightMode = sharedPref.getBoolean(SettingsActivity.KEY_NIGHT_MODE, true);
        if (nightMode != isNightMode) { // Restart activity to apply night mode
            Intent intent = getIntent();
            intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TOP); // Prevent loops in back button
            finish();
            startActivity(intent);
        }
        if (agendaButton != null)
            generateAgendaMenu(agendaButton.getSubMenu());
    }

    /**
     * Displays the edit subjects button if the
     * {@link com.clubinfo.insat.memorisia.fragments.SubjectsFragment SubjectsFragment} or
     * {@link WorkViewFragment WorkViewFragment}
     * are active
     */
    private void checkEditButtonState() {
        if (isFragmentActive(Frags.FRAG_SUBJECTS) || isFragmentActive(Frags.FRAG_WORK_TYPES) || isFragmentActive(Frags.FRAG_WORKS)) {
            editButton.setVisible(true);
        } else {
            editButton.setVisible(false);
        }
    }

    /**
     * Displays the sort button if the
     * {@link com.clubinfo.insat.memorisia.fragments.SubjectsFragment SubjectsFragment} or
     * {@link WorkViewFragment WorkViewFragment} or
     * {@link com.clubinfo.insat.memorisia.fragments.CalendarFragment CalendarFragment}
     * are active
     */
    public void checkSortButtonState() {
        if (isFragmentActive(Frags.FRAG_SUBJECTS) || isFragmentActive(Frags.FRAG_WORKS)
                || isFragmentActive(Frags.FRAG_WORK_TYPES) || isFragmentActive(Frags.FRAG_CALENDAR)) {
            sortButton.setVisible(true);
        } else {
            sortButton.setVisible(false);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (activeFragment == Frags.FRAG_HOME) {
            drawer.openDrawer(GravityCompat.START);
        } else if (activeFragment != Frags.FRAG_WORKS) {
            startFragment(Frags.FRAG_HOME);
        } else {
            if (backStackFragment != null) { // Restore active fragment on back
                activeFragment = backStackFragment;
                backStackFragment = null;
            }
            super.onBackPressed();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu = m;
        getMenuInflater().inflate(R.menu.main, menu);
        editButton = menu.findItem(R.id.action_edit);
        sortButton = menu.findItem(R.id.action_sort);
        agendaButton = menu.findItem(R.id.action_agenda);
        checkEditButtonState();
        checkSortButtonState();
        Utils.setToolbarIconWhite(editButton);
        Utils.setToolbarIconWhite(sortButton);
        Utils.setToolbarIconWhite(agendaButton);

        generateSortMenu(isFragmentActive(Frags.FRAG_WORKS) || isFragmentActive(Frags.FRAG_CALENDAR) || isFragmentActive(Frags.FRAG_ALL));

        generateAgendaMenu(agendaButton.getSubMenu());
        return true;
    }

    /**
     * Generates the corresponding sort menu for the active fragment
     *
     * @param isWorks Is the menu for subjects sorting or work sorting?
     */
    public void generateSortMenu(boolean isWorks) {
        Menu subMenu = sortButton.getSubMenu();
        subMenu.clear();
        subMenu.add(0, R.id.sort_1, Menu.NONE, R.string.sort_name);
        if (isWorks) {
            subMenu.add(0, R.id.sort_2, Menu.NONE, R.string.sort_priority);
            subMenu.add(0, R.id.sort_3, Menu.NONE, R.string.sort_type);
            subMenu.add(0, R.id.sort_4, Menu.NONE, R.string.sort_date);
        } else {
            subMenu.add(0, R.id.sort_2, Menu.NONE, R.string.sort_percent);
            subMenu.add(0, R.id.sort_3, Menu.NONE, R.string.sort_total_work);
        }
//        if (activeFragment != Frags.FRAG_HOME) {
//            BaseFragment frag = (BaseFragment) getFragmentManager().findFragmentByTag(activeFragment.name());
////            changeSortMenuItemIcon(subMenu.getItem(frag.getCurrentSortType().ordinal()), frag.isReverseSort());
//        }
    }

    /**
     * Generates the agenda menu in the toolbar allowing the user to select which ones to display
     *
     * @param menu Menu holding the agendas
     */
    public void generateAgendaMenu(final Menu menu) {
        selectedAgendas = Utils.getSelectedAgendasFromPrefs(context);
        List<OptionModule> modules = MemorisiaDatabase.getInstance(context).optionModuleDao().getOptionModulesOfType(OptionModule.AGENDA);
        menu.clear();
        for (int i = 0; i < modules.size(); i++) {
            MenuItem item = menu.add(0, modules.get(i).getId(), Menu.NONE, modules.get(i).getText());
            item.setCheckable(true);
            item.setChecked(selectedAgendas.contains(modules.get(i).getId())); // Check the entry if user previously selected it
            // Update the selected agendas list on click and re-generate the corresponding list
            item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    if (!menuItem.isChecked()) {
                        menuItem.setChecked(true);
                        selectedAgendas.add(menuItem.getItemId());
                    } else if (canDeselectAgenda(menuItem.getItemId())) {
                        for (int i = 0; i < selectedAgendas.size(); i++) {
                            if (selectedAgendas.get(i) == menuItem.getItemId())
                                selectedAgendas.remove(i);
                        }
                        menuItem.setChecked(false);
                    }
                    Utils.saveSelectedAgendasToPrefs(context, selectedAgendas);
                    SubjectsFragment subjects = (SubjectsFragment) getFragmentManager().findFragmentByTag(Frags.FRAG_SUBJECTS.name());
                    WorkViewFragment works = (WorkViewFragment) getFragmentManager().findFragmentByTag(Frags.FRAG_WORKS.name());
                    WorkTypesFragment workTypes = (WorkTypesFragment) getFragmentManager().findFragmentByTag(Frags.FRAG_WORK_TYPES.name());
                    CalendarFragment calendar = (CalendarFragment) getFragmentManager().findFragmentByTag(Frags.FRAG_CALENDAR.name());
                    HomeFragment home = (HomeFragment) getFragmentManager().findFragmentByTag(Frags.FRAG_HOME.name());
                    AllWorksFragment all = (AllWorksFragment) getFragmentManager().findFragmentByTag(Frags.FRAG_ALL.name());
                    if (isFragmentActive(Frags.FRAG_SUBJECTS))
                        subjects.generateList();
                    else if (isFragmentActive(Frags.FRAG_WORKS))
                        works.generateList();
                    else if (isFragmentActive(Frags.FRAG_WORK_TYPES))
                        workTypes.generateList();
                    else if (isFragmentActive(Frags.FRAG_CALENDAR))
                        calendar.generateList();
                    else if (isFragmentActive(Frags.FRAG_HOME))
                        home.generateTabs();
                    else if (isFragmentActive(Frags.FRAG_ALL))
                        all.generateList();

                    return false;
                }
            });
        }
    }

    /**
     * Checks if the selected agenda in the menu is the only one active.
     * If it is the only one, prevents the user from deselecting it.
     *
     * @param id Agenda id
     * @return True if the user can deselect the agenda, false otherwise
     */
    private boolean canDeselectAgenda(int id) {
        Menu menu = agendaButton.getSubMenu();
        for (int i = 0; i < menu.size(); i++) {
            if (menu.getItem(i).isChecked() && menu.getItem(i).getItemId() != id)
                return true;
        }
        return false;
    }

    /**
     * @return Reference to the {@link android.view.MenuItem MenuItem} holding the sort options
     */
    public MenuItem getSortButton() {
        return sortButton;
    }

    /**
     * @return List of the selected agendas
     */
    public List<Integer> getSelectedAgendas() {
        return selectedAgendas;
    }

    /**
     * @param id id of the agenda to select
     */
    public void addAgendaToSelected(int id) {
        selectedAgendas.add(id);
        Utils.saveSelectedAgendasToPrefs(this, selectedAgendas);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        BaseFragment.SortType sort = BaseFragment.SortType.SORT_1;
        SubjectsFragment subjects = (SubjectsFragment) getFragmentManager().findFragmentByTag(Frags.FRAG_SUBJECTS.name());
        WorkViewFragment works = (WorkViewFragment) getFragmentManager().findFragmentByTag(Frags.FRAG_WORKS.name());
        WorkTypesFragment workTypes = (WorkTypesFragment) getFragmentManager().findFragmentByTag(Frags.FRAG_WORK_TYPES.name());
        CalendarFragment calendar = (CalendarFragment) getFragmentManager().findFragmentByTag(Frags.FRAG_CALENDAR.name());
        AllWorksFragment all = (AllWorksFragment) getFragmentManager().findFragmentByTag(Frags.FRAG_ALL.name());
        switch (id) {
            case R.id.action_edit:
                if (isFragmentActive(Frags.FRAG_WORKS))
                    editCurrentOptionModule(works.getParentId());
                else if (isFragmentActive(Frags.FRAG_SUBJECTS))
                    editModules(OptionModule.SUBJECT);
                else if (isFragmentActive(Frags.FRAG_WORK_TYPES))
                    editModules(OptionModule.WORK_TYPE);
                break;
            case R.id.sort_1:
                sort = BaseFragment.SortType.SORT_1;
                break;
            case R.id.sort_2:
                sort = BaseFragment.SortType.SORT_2;
                break;
            case R.id.sort_3:
                sort = BaseFragment.SortType.SORT_3;
                break;
            case R.id.sort_4:
                sort = BaseFragment.SortType.SORT_4;
                break;
        }
        if ((id == R.id.sort_1 || id == R.id.sort_2 || id == R.id.sort_3 || id == R.id.sort_4)) {
            if (isFragmentActive(Frags.FRAG_SUBJECTS)) {
                subjects.setSortType(sort, true);
                changeSortMenuItemIcon(item, subjects.isReverseSort());
            } else if (isFragmentActive(Frags.FRAG_WORK_TYPES)) {
                workTypes.setSortType(sort, true);
                changeSortMenuItemIcon(item, workTypes.isReverseSort());
            } else if (isFragmentActive(Frags.FRAG_WORKS)) {
                works.setSortType(sort, false);
                changeSortMenuItemIcon(item, works.isReverseSort());
            } else if (isFragmentActive(Frags.FRAG_CALENDAR)) {
                calendar.setSortType(sort, false);
                changeSortMenuItemIcon(item, calendar.isReverseSort());
            } else if (isFragmentActive(Frags.FRAG_ALL)){
                all.setSortType(sort, false);
                changeSortMenuItemIcon(item, all.isReverseSort());
            }
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Creates a new {@link com.clubinfo.insat.memorisia.activities.EditOptionsActivity EditOptionsActivity}
     * corresponding to the given subject.
     *
     * @param id Id of the option module to edit
     */
    private void editCurrentOptionModule(int id) {
        Intent intent = new Intent(this, EditOptionsActivity.class);
        Bundle b = new Bundle();
        b.putInt("id", id);
        intent.putExtras(b);
        startActivity(intent);
    }

    /**
     * Creates a new {@link com.clubinfo.insat.memorisia.activities.OptionsListActivity OptionsListActivity}
     */
    private void editModules(int type) {
        Intent intent = new Intent(this, OptionsListActivity.class);
        intent.setData(Uri.parse(type + ""));
        startActivity(intent);
    }

    /**
     * Changes the icon of a given MenuItem to display the current sorting order
     *
     * @param item      Item that will have its icon changed
     * @param isReverse True to make the arrow point upward, false for downward
     */
    public void changeSortMenuItemIcon(MenuItem item, boolean isReverse) {
        for (int i = 0; i < sortButton.getSubMenu().size(); i++) {
            MenuItem it = sortButton.getSubMenu().getItem(i);
            if (it.getItemId() != item.getItemId())
                it.setIcon(0);
            if (!isReverse)
                item.setIcon(R.drawable.ic_arrow_downward_black_24dp);
            else
                item.setIcon(R.drawable.ic_arrow_upward_black_24dp);
        }
    }

    private void startFragment(Frags type) {
        Fragment fragment = new HomeFragment();
        activeFragment = type;
        editButton.setVisible(false);
        sortButton.setVisible(false);
        NavigationView navigationView = findViewById(R.id.nav_view);
        switch (type) {
            case FRAG_HOME:
                fragment = new HomeFragment();
                findViewById(R.id.tabs).setVisibility(View.VISIBLE);
                navigationView.setCheckedItem(R.id.nav_home);
                break;
            case FRAG_SUBJECTS:
                fragment = new SubjectsFragment();
                editButton.setVisible(true);
                sortButton.setVisible(true);
                findViewById(R.id.tabs).setVisibility(View.GONE);
                navigationView.setCheckedItem(R.id.nav_subjects);
                break;
            case FRAG_WORK_TYPES:
                fragment = new WorkTypesFragment();
                editButton.setVisible(true);
                sortButton.setVisible(true);
                findViewById(R.id.tabs).setVisibility(View.GONE);
                navigationView.setCheckedItem(R.id.nav_work_types);
                break;
            case FRAG_CALENDAR:
                fragment = new CalendarFragment();
                editButton.setVisible(false);
                sortButton.setVisible(true);
                findViewById(R.id.tabs).setVisibility(View.GONE);
                navigationView.setCheckedItem(R.id.nav_calendar);
                break;
            case FRAG_ALL:
                fragment = new AllWorksFragment();
                editButton.setVisible(false);
                sortButton.setVisible(true);
                findViewById(R.id.tabs).setVisibility(View.GONE);
                navigationView.setCheckedItem(R.id.nav_all);
        }
        // Remove backstack to prevent problems when pressing back button
        FragmentManager fm = getFragmentManager();
        // Pop the backstack before and after to prevent problems with work view
        fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out, android.R.animator.fade_in, android.R.animator.fade_out);
        ft.replace(R.id.content_frame, fragment, activeFragment.name());
        ft.commit();
        fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Frags type = null;
        switch (id) {
            case R.id.nav_home:
                type = Frags.FRAG_HOME;
                break;
            case R.id.nav_subjects:
                type = Frags.FRAG_SUBJECTS;
                break;
            case R.id.nav_work_types:
                type = Frags.FRAG_WORK_TYPES;
                break;
            case R.id.nav_calendar:
                type = Frags.FRAG_CALENDAR;
                break;
            case R.id.nav_all:
                type = Frags.FRAG_ALL;
                break;
        }
        if (type != null) {
            startFragment(type);
        } else if (id == R.id.nav_settings) { // Display the settings activity
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.nav_about) { // Display the about activity
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public boolean isFragmentActive(Frags Frag) {
        return activeFragment == Frag;
    }

    public enum Frags {
        FRAG_HOME,
        FRAG_SUBJECTS,
        FRAG_WORKS,
        FRAG_WORK_TYPES,
        FRAG_CALENDAR,
        FRAG_ALL
    }

}
