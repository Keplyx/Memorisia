/*
 * Copyright (c) 2018.
 * This file is part of Memorisia.
 *
 * Memorisia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Memorisia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Memorisia.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.clubinfo.insat.memorisia;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import java.util.List;

public class CalendarEventDecorator implements DayViewDecorator {

    private final List<CalendarDay> dates;
    private TypedValue todayColor = new TypedValue();

    public CalendarEventDecorator(List<CalendarDay> dates) {
        this.dates = dates;
    }

    public CalendarEventDecorator(Context context) {
        this.dates = null;
        context.getTheme().resolveAttribute(R.attr.colorAccent, todayColor, true);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        if (dates != null)
            return dates.contains(day);
        else
            return day.equals(CalendarDay.today());
    }

    @Override
    public void decorate(DayViewFacade view) {
        if (dates != null)
            view.addSpan(new DotSpan(5, Color.RED));
        else
            view.addSpan(new DotSpan(10, todayColor.data));
    }
}
