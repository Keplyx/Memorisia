/*
 * Copyright (c) 2018.
 * This file is part of Memorisia.
 *
 * Memorisia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Memorisia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Memorisia.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.clubinfo.insat.memorisia.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clubinfo.insat.memorisia.R;
import com.clubinfo.insat.memorisia.activities.MainActivity;
import com.clubinfo.insat.memorisia.activities.SettingsActivity;
import com.clubinfo.insat.memorisia.adapters.CollectionPagerAdapter;


public class HomeFragment extends Fragment {

    private FragmentActivity context;
    ViewPager viewPager;
    TabLayout tabLayout;
    private int lastTabIndex = -1;

    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_fragment, container, false);
        getActivity().setTitle(R.string.home_title);

        viewPager = v.findViewById(R.id.viewPager);
        tabLayout = context.findViewById(R.id.tabs);
        generateTabs();
        return v;
    }

    public void generateTabs(){
        CollectionPagerAdapter collectionPagerAdapter = new CollectionPagerAdapter(context.getSupportFragmentManager(), context);
        viewPager.setAdapter(collectionPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        final MainActivity act = (MainActivity) getActivity();
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                act.getFabButton().show();
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setSelectedTab();
    }

    public void setSelectedTab() {
        String[] aliasArray = context.getResources().getStringArray(R.array.start_tab_choices_alias);
        String selected = PreferenceManager.getDefaultSharedPreferences(context).getString(SettingsActivity.KEY_START_TAB, aliasArray[0]);

        for (int i = 0; i < aliasArray.length; i++){
            if (selected.equals(aliasArray[i]))
                viewPager.setCurrentItem(i);
        }
    }

    private void resumeLastTab(){
        TabLayout.Tab tab = tabLayout.getTabAt(lastTabIndex);
        if (tab != null) {
            tab.select();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        lastTabIndex = tabLayout.getSelectedTabPosition();
        generateTabs();
        resumeLastTab();
    }
}
