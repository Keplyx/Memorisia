package com.clubinfo.insat.memorisia;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.clubinfo.insat.memorisia.activities.MainActivity;
import com.clubinfo.insat.memorisia.activities.SettingsActivity;
import com.clubinfo.insat.memorisia.adapters.WorksRecyclerAdapter;
import com.clubinfo.insat.memorisia.database.MemorisiaDatabase;
import com.clubinfo.insat.memorisia.modules.WorkModule;
import com.clubinfo.insat.memorisia.utils.ModulesUtils;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.util.ArrayList;
import java.util.List;

public class DemoObjectFragment extends Fragment {
    // Instances of this class are fragments representing a single
// object in our collection.
    public static final String ARG_OBJECT = "tab";

    private RecyclerView recyclerView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v;
        Bundle args = getArguments();
        if (args != null && args.getInt(ARG_OBJECT) == 0)
            v = initWebView(inflater, container);
        else if (args != null && args.getInt(ARG_OBJECT) == 1) {
            v = initRecyclerView(inflater, container);
            generateWeekList();
        } else {
            v = initRecyclerView(inflater, container);
            generateStarsList();
        }
        return v;
    }

    public View initWebView(LayoutInflater inflater, ViewGroup container) {
        View v = inflater.inflate(R.layout.webview_layout, container, false);
        WebView webView = v.findViewById(R.id.home_webview);
        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(android.R.attr.colorBackground, typedValue, true);
        if (typedValue.type >= TypedValue.TYPE_FIRST_COLOR_INT && typedValue.type <= TypedValue.TYPE_LAST_COLOR_INT) {
            // windowBackground is a color
            webView.setBackgroundColor(typedValue.data);
        }
        WebSettings webSettings = webView.getSettings();
        webSettings.setDomStorageEnabled(true);
        webSettings.setJavaScriptEnabled(true);
        String url = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(SettingsActivity.KEY_WEB_FAV_URL, "");
        webView.loadUrl(url);
        return v;
    }

    public View initRecyclerView(LayoutInflater inflater, ViewGroup container) {
        // The last two arguments ensure LayoutParams are inflated
        // properly.
        View v = inflater.inflate(R.layout.recyclerview_layout, container, false);

        recyclerView = v.findViewById(R.id.subjectsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                MainActivity act = (MainActivity) getActivity();
                if (act != null) {
                    if (dy > 0)
                        act.getFabButton().hide();
                    else if (dy < 0)
                        act.getFabButton().show();
                }

            }
        });
        return v;
    }

    public void generateWeekList() {
        MemorisiaDatabase db = MemorisiaDatabase.getInstance(getActivity());
        MainActivity act = (MainActivity) getActivity();
        List<WorkModule> worksList = new ArrayList<>();
        if (act != null)
            worksList = ModulesUtils.getWorkModuleListByWeek(
                    db.workModuleDao().getWorkModulesOfAgenda(act.getSelectedAgendas()), new int[]{CalendarDay.today().getDay(), CalendarDay.today().getMonth() + 1, CalendarDay.today().getYear()});
        RecyclerView.Adapter mAdapter;
        mAdapter = new WorksRecyclerAdapter(getActivity(), ModulesUtils.sortWorkModuleListByDate(worksList, false));
        recyclerView.setAdapter(mAdapter);

    }

    public void generateStarsList() {
        MemorisiaDatabase db = MemorisiaDatabase.getInstance(getActivity());
        MainActivity act = (MainActivity) getActivity();
        List<WorkModule> worksList = new ArrayList<>();
        if (act != null)
            worksList = ModulesUtils.getWorkModuleListByPriority(
                    db.workModuleDao().getWorkModulesOfAgenda(act.getSelectedAgendas()), 5);
        RecyclerView.Adapter mAdapter;
        mAdapter = new WorksRecyclerAdapter(getActivity(), ModulesUtils.sortWorkModuleListByDate(worksList, false));
        recyclerView.setAdapter(mAdapter);
    }
}
