package com.clubinfo.insat.memorisia.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.clubinfo.insat.memorisia.DemoObjectFragment;
import com.clubinfo.insat.memorisia.R;

public class CollectionPagerAdapter extends FragmentStatePagerAdapter {
    private Context myContext;

    public CollectionPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        myContext = context;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = new DemoObjectFragment();
        Bundle args = new Bundle();
        args.putInt(DemoObjectFragment.ARG_OBJECT, i);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String name;
        switch (position) {
            case 0:
                name = myContext.getString(R.string.home_fav);
                break;
            case 1:
                name = myContext.getString(R.string.home_week);
                break;
            default:
                name = myContext.getString(R.string.home_stars);
                break;
        }
        return name;
    }

}