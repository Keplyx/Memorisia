/*
 * Copyright (c) 2018.
 * This file is part of Memorisia.
 *
 * Memorisia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Memorisia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Memorisia.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.clubinfo.insat.memorisia.adapters;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.clubinfo.insat.memorisia.R;
import com.clubinfo.insat.memorisia.activities.EditWorkActivity;
import com.clubinfo.insat.memorisia.database.MemorisiaDatabase;
import com.clubinfo.insat.memorisia.modules.OptionModule;
import com.clubinfo.insat.memorisia.modules.WorkModule;
import com.clubinfo.insat.memorisia.utils.ModulesUtils;
import com.clubinfo.insat.memorisia.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class WorksRecyclerAdapter extends RecyclerView.Adapter<WorksRecyclerAdapter.ViewHolder> {

    private TypedValue outdatedColor = new TypedValue();
    private TypedValue tomorrowColor = new TypedValue();
    private TypedValue thisWeekColor = new TypedValue();
    private TypedValue normalColor = new TypedValue();

    private List<WorkModule> modules;
    private List<OptionModule> workTypesList;
    private List<OptionModule> subjectsList;
    private List<OptionModule> agendaList;
    private Context context;
    private int mExpandedPosition = -1;
    // boolean array for checking the state of works (prevents unwanted unchecking when recycling)
    private List<Boolean> worksStateArray = new ArrayList<>();

    public WorksRecyclerAdapter(Context context, List<WorkModule> modules) {
        this.context = context;
        this.modules = modules;
        for (int i = 0; i < modules.size(); i++) {
            worksStateArray.add(modules.get(i).isState());
        }
        MemorisiaDatabase db = MemorisiaDatabase.getInstance(context);
        workTypesList = db.optionModuleDao().getOptionModulesOfType(OptionModule.WORK_TYPE);
        subjectsList = db.optionModuleDao().getOptionModulesOfType(OptionModule.SUBJECT);
        agendaList = db.optionModuleDao().getOptionModulesOfType(OptionModule.AGENDA);
        getColorsFromTheme();
    }

    private void getColorsFromTheme() {
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(R.attr.colorOutdatedWork, outdatedColor, true);
        theme.resolveAttribute(R.attr.colortomorowWork, tomorrowColor, true);
        theme.resolveAttribute(R.attr.colorthisWeekWork, thisWeekColor, true);
        theme.resolveAttribute(R.attr.colornormalWork, normalColor, true);
    }

    public void add(int pos, WorkModule item) {
        modules.add(pos, item);
        notifyItemInserted(pos);
    }

    private void removeItem(int pos) {
        modules.remove(pos);
        notifyItemRemoved(pos);
        worksStateArray.remove(pos);
        notifyItemRangeChanged(pos, modules.size());

    }

    @NonNull
    @Override
    public WorksRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v;
        if (modules.size() != 0)
            v = inflater.inflate(R.layout.workslist_row_item, parent, false);
        else
            v = inflater.inflate(R.layout.empty_list, parent, false);
        return new WorksRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final WorksRecyclerAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int pos) {
        if (modules.size() == 0)
            return;
        final WorkModule work = modules.get(pos);
        holder.work = work;
        holder.pos = pos;
        OptionModule workType = ModulesUtils.getModuleOfId(workTypesList, work.getWorkTypeId());
        OptionModule subject = ModulesUtils.getModuleOfId(subjectsList, work.getSubjectId());
        OptionModule agenda = ModulesUtils.getModuleOfId(agendaList, work.getAgendaId());

        if (workType != null) {
            holder.type.setText(workType.getText());
            holder.workLogo.setImageBitmap(Utils.getBitmapFromAsset(context, workType.getLogo()));
            holder.workLogo.setColorFilter(Color.parseColor(workType.getColor()));
        }
        if (subject != null) {
            holder.subject.setText(subject.getText());
            holder.subjectLogo.setImageBitmap(Utils.getBitmapFromAsset(context, subject.getLogo()));
            holder.subjectLogo.setColorFilter(Color.parseColor(subject.getColor()));
        }
        if (agenda != null) {
            holder.agenda.setText(agenda.getText());
            holder.agendaLogo.setImageBitmap(Utils.getBitmapFromAsset(context, agenda.getLogo()));
            holder.agendaLogo.setColorFilter(Color.parseColor(agenda.getColor()));
            holder.mainLogo.setImageBitmap(Utils.getBitmapFromAsset(context, agenda.getLogo()));
            holder.mainLogo.setColorFilter(Color.parseColor(agenda.getColor()));
        }


        Calendar c = Calendar.getInstance();
        int[] today = new int[]{c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.MONTH) + 1, c.get(Calendar.YEAR)};
        int delta = Utils.getDateDelta(today, work.getDate());
        if (delta <= 0)
            setupDateDisplay(holder.date, outdatedColor, holder.dateImage, work.getDate());
        else if (delta <= 1)
            setupDateDisplay(holder.date, tomorrowColor, holder.dateImage, work.getDate());
        else if (delta <= 7)
            setupDateDisplay(holder.date, thisWeekColor, holder.dateImage, work.getDate());
        else
            setupDateDisplay(holder.date, normalColor, holder.dateImage, work.getDate());

        if (delta == 0) {
            int current = c.get(Calendar.MINUTE) + c.get(Calendar.HOUR_OF_DAY) * 60;
            int due = work.getTime()[1] + work.getTime()[0] * 60;
            if (current < due)
                setupTimeDisplay(holder.time, tomorrowColor, work.getTime());
            else
                setupTimeDisplay(holder.time, outdatedColor, work.getTime());
        } else if (delta < 0) {
            setupTimeDisplay(holder.time, outdatedColor, work.getTime());
        } else {
            setupTimeDisplay(holder.time, normalColor, work.getTime());
        }

        holder.title.setText(work.getText());
        holder.priorityBar.setRating((float) work.getPriority());
        holder.doneCheckBox.setChecked(worksStateArray.get(pos));
        setWorkChecked(holder, worksStateArray.get(pos));
        holder.doneCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                worksStateArray.set(pos, !worksStateArray.get(pos));
                modules.get(pos).setState(worksStateArray.get(pos));
                setWorkChecked(holder, worksStateArray.get(pos));
                MemorisiaDatabase.getInstance(context).workModuleDao().updateWorkModules(work);
            }
        });
        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editSelectedWork(work);
            }
        });
        setupDetails(holder, pos);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setupDetails(ViewHolder holder, final int pos) {
        final boolean isExpanded = (pos == mExpandedPosition);
        int rowDimId = isExpanded ? R.dimen.worklist_row_active : R.dimen.worklist_row_normal;
        int titleDimId = isExpanded ? R.dimen.worktitle_active : R.dimen.worktitle_normal;
        holder.layout.getLayoutParams().height = (int) context.getResources().getDimension(rowDimId);
        holder.title.getLayoutParams().height = (int) context.getResources().getDimension(titleDimId);
        holder.title.setMaxLines(isExpanded ? 3 : 1);
        holder.agendaLogo.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.agenda.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.editButton.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.itemView.setActivated(isExpanded);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mExpandedPosition != -1)
                    notifyItemChanged(mExpandedPosition); // Close last selected
                mExpandedPosition = isExpanded ? -1 : pos;
                if (mExpandedPosition != -1)
                    notifyItemChanged(mExpandedPosition); // Open new selected
            }
        });
    }

    @Override
    public void onViewRecycled(@NonNull ViewHolder holder) {
        holder.itemView.setOnLongClickListener(null);
        super.onViewRecycled(holder);
    }

    /**
     * Sets the Drawable for the date textView based on the due date
     *
     * @param text  textView used to display the date
     * @param color Drawable color
     * @param date  Work due date
     */
    private void setupDateDisplay(TextView text, TypedValue color, ImageView dateImage, int[] date) {
        if (date[0] != -1) {
            text.setText(Utils.getDateText(date));
            text.setTextColor(color.data);
            dateImage.setColorFilter(color.data);
        } else {
            text.setText("");
            text.setCompoundDrawables(null, null, null, null);
            dateImage.setVisibility(View.GONE);
        }
    }


    /**
     * Sets the Drawable for the time textView based on the due date
     *
     * @param text  textView used to display the time
     * @param color Drawable color
     * @param time  Work due date
     */
    private void setupTimeDisplay(TextView text, TypedValue color, int[] time) {
        if (time[0] != -1) {
            text.setText(Utils.getTimeText(time));
            text.setTextColor(color.data);
        } else {
            text.setText("");
            text.setCompoundDrawables(null, null, null, null);
        }
    }

    /**
     * Starts a new EditWorkActivity to let the user edit the selected work
     *
     * @param work Selected WorkModule
     */
    private void editSelectedWork(WorkModule work) {
        if (work != null) {
            Intent intent = new Intent(context, EditWorkActivity.class);
            Bundle b = ModulesUtils.createBundleFromModule(work);
            intent.putExtras(b);
            context.startActivity(intent);
        }

    }

    private void showConfirmDeleteDialog(final WorkModule work, final int pos) {
        ContextThemeWrapper ctw = new ContextThemeWrapper(context, R.style.AppTheme);
        final AlertDialog.Builder builder = new AlertDialog.Builder(ctw);
        builder.setMessage(context.getResources().getString(R.string.confirm_delete));
        builder.setCancelable(true);
        builder.setPositiveButton(
                context.getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        MemorisiaDatabase.getInstance(builder.getContext()).workModuleDao().deleteWorkModules(work);
                        removeItem(pos);
                        dialog.cancel();
                    }
                });

        builder.setNegativeButton(
                context.getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Makes the selected view turn green if it is selected (based on current theme), normal if not
     *
     * @param holder    ViewHolder of the selected work
     * @param isChecked Whether the view is selected or not
     */
    private void setWorkChecked(ViewHolder holder, boolean isChecked) {
        TypedValue cardBackValue = new TypedValue();
        TypedValue cardTitleValue = new TypedValue();
        if (!isChecked) {
            context.getTheme().resolveAttribute(R.attr.colorCardBackground, cardBackValue, true);
            context.getTheme().resolveAttribute(R.attr.colorCardTitleBackground, cardTitleValue, true);
        } else {
            context.getTheme().resolveAttribute(R.attr.colorWorkDone, cardBackValue, true);
            context.getTheme().resolveAttribute(R.attr.colorWorkTitleDone, cardTitleValue, true);
        }
        holder.cardContainer.setCardBackgroundColor(cardBackValue.data);
        holder.titleCard.setCardBackgroundColor(cardTitleValue.data);
    }

    @Override
    public int getItemCount() {
        if (modules.size() != 0)
            return modules.size();
        else
            return 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final View.OnCreateContextMenuListener mOnCreateContextMenuListener = new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                MenuInflater inflater = new MenuInflater(context);
                inflater.inflate(R.menu.worklist_context_menu, menu);
                MenuItem editItem = menu.getItem(0);
                MenuItem deleteItem = menu.getItem(1);

                editItem.setOnMenuItemClickListener(mOnMyActionClickListener);
                deleteItem.setOnMenuItemClickListener(mOnMyActionClickListener);
            }
        };
        TextView title;
        TextView type;
        TextView subject;
        TextView agenda;
        TextView date;
        TextView time;
        ImageView workLogo;
        ImageView subjectLogo;
        ImageView agendaLogo;
        ImageView mainLogo;
        ImageView dateImage;
        RatingBar priorityBar;
        CheckBox doneCheckBox;
        CardView cardContainer;
        CardView titleCard;
        View layout;
        ImageButton editButton;
        WorkModule work = null;
        int pos = -1;
        private final MenuItem.OnMenuItemClickListener mOnMyActionClickListener = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.editItem:
                        editSelectedWork(work);
                        break;
                    case R.id.deleteItem:
                        showConfirmDeleteDialog(work, pos);
                        break;
                }
                return true;
            }
        };

        ViewHolder(View v) {
            super(v);
            layout = v;
            if (modules.size() != 0) {
                title = v.findViewById(R.id.workTitle);
                type = v.findViewById(R.id.workTypeTextView);
                subject = v.findViewById(R.id.SubjectTextView);
                agenda = v.findViewById(R.id.AgendaTextView);

                date = v.findViewById(R.id.dateTextView);
                time = v.findViewById(R.id.timeTextView);
                workLogo = v.findViewById(R.id.workLogo);
                subjectLogo = v.findViewById(R.id.subjectLogo);
                agendaLogo = v.findViewById(R.id.agendaLogo);
                mainLogo = v.findViewById(R.id.mainLogo);
                dateImage = v.findViewById(R.id.dateImage);
                priorityBar = v.findViewById(R.id.priorityRatingBar);
                doneCheckBox = v.findViewById(R.id.stateCheckBox);
                cardContainer = v.findViewById(R.id.cardContainer);
                titleCard = v.findViewById(R.id.titleCard);
                editButton = v.findViewById(R.id.editButton);

                v.setOnCreateContextMenuListener(mOnCreateContextMenuListener);
            }
        }
    }

}
