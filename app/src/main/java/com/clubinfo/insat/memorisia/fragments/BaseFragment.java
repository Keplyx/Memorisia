/*
 * Copyright (c) 2018.
 * This file is part of Memorisia.
 *
 * Memorisia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Memorisia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Memorisia.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.clubinfo.insat.memorisia.fragments;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clubinfo.insat.memorisia.R;
import com.clubinfo.insat.memorisia.activities.MainActivity;
import com.clubinfo.insat.memorisia.activities.SettingsActivity;

public class BaseFragment extends Fragment {

    private SortType currentSortType = SortType.SORT_1;
    private boolean reverseSort = false;
    private RecyclerView recyclerView;

    public SortType getCurrentSortType() {
        return currentSortType;
    }

    public void setCurrentSortType(SortType value) {
        currentSortType = value;
    }

    public boolean isReverseSort() {
        return reverseSort;
    }

    public void setReverseSort(boolean value) {
        reverseSort = value;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    protected View inflateLayout(LayoutInflater inflater, ViewGroup container, boolean isWorkView) {
        View view = inflater.inflate(R.layout.recyclerview_layout, container, false);
        recyclerView = view.findViewById(R.id.subjectsRecyclerView);
        setupRecyclerView(recyclerView);
        initFragment(isWorkView);

        return view;
    }

    protected void initFragment(boolean isWorkView) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        setCurrentSortType(SortType.values()[sharedPref.getInt(SettingsActivity.KEY_SUBJECTS_SORT_TYPE, 0)]);
        setReverseSort(sharedPref.getBoolean(SettingsActivity.KEY_SUBJECTS_SORT_REVERSE, false));
        resumeSortType(isWorkView);
        MainActivity act = (MainActivity) getActivity();
        if (act.getSortButton() != null) {
            act.generateSortMenu(isWorkView);
            act.changeSortMenuItemIcon(act.getSortButton().getSubMenu().getItem(getCurrentSortType().ordinal()), isReverseSort());
        }
        act.getFabButton().show();

    }

    protected void setupRecyclerView(RecyclerView recyclerView) {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                MainActivity act = (MainActivity) getActivity();
                if (dy > 0)
                    act.getFabButton().hide();
                else if (dy < 0)
                    act.getFabButton().show();
            }
        });
    }

    private void resumeSortType(boolean isWorkView) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (isWorkView) {
            int sortType = sharedPref.getInt(SettingsActivity.KEY_WORKS_SORT_TYPE, 0);
            currentSortType = SortType.values()[sortType];
            reverseSort = sharedPref.getBoolean(SettingsActivity.KEY_WORKS_SORT_REVERSE, false);
        } else {
            int sortType = sharedPref.getInt(SettingsActivity.KEY_SUBJECTS_SORT_TYPE, 0);
            currentSortType = SortType.values()[sortType];
            reverseSort = sharedPref.getBoolean(SettingsActivity.KEY_SUBJECTS_SORT_REVERSE, false);
        }
    }

    /**
     * Sets the sort type to use for the list, then saves it to the shared prefs
     *
     * @param type Sort type
     */
    public void setSortType(SortType type, boolean isSubjects) {
        reverseSort = type == currentSortType && !reverseSort;
        currentSortType = type;
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = sharedPref.edit();
        if (isSubjects) {
            editor.putInt(SettingsActivity.KEY_SUBJECTS_SORT_TYPE, currentSortType.ordinal());
            editor.putBoolean(SettingsActivity.KEY_SUBJECTS_SORT_REVERSE, reverseSort);
        } else {
            editor.putInt(SettingsActivity.KEY_WORKS_SORT_TYPE, currentSortType.ordinal());
            editor.putBoolean(SettingsActivity.KEY_WORKS_SORT_REVERSE, reverseSort);
        }
        editor.apply();
        generateList();
    }

    public void generateList() {

    }

    @Override
    public void onResume() {
        super.onResume();
        generateList();
        MainActivity act = (MainActivity) getActivity();
        if (act.getActiveFragment() != MainActivity.Frags.FRAG_HOME) // Prevent tab space from being created when resuming the activity
            act.findViewById(R.id.tabs).setVisibility(View.GONE);
    }

    public enum SortType {
        SORT_1,
        SORT_2,
        SORT_3,
        SORT_4
    }

}
