/*
 * Copyright (c) 2018.
 * This file is part of Memorisia.
 *
 * Memorisia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Memorisia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Memorisia.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.clubinfo.insat.memorisia.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.clubinfo.insat.memorisia.R;
import com.clubinfo.insat.memorisia.adapters.CustomSpinnerAdapter;
import com.clubinfo.insat.memorisia.adapters.LogosListAdapter;
import com.clubinfo.insat.memorisia.database.MemorisiaDatabase;
import com.clubinfo.insat.memorisia.modules.Module;
import com.clubinfo.insat.memorisia.modules.OptionModule;
import com.clubinfo.insat.memorisia.modules.WorkModule;
import com.clubinfo.insat.memorisia.utils.ModulesUtils;
import com.clubinfo.insat.memorisia.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import yuku.ambilwarna.AmbilWarnaDialog;

public class EditOptionsActivity extends AppCompatActivity {

    private Context context;
    private TextView title;
    private GridView logosGridView;
    private Button colorButton;
    private Button deleteButton;
    private Switch agendaSwitch;
    private Spinner agendaSpinner;
    private Switch subjectSwitch;
    private Spinner subjectSpinner;

    private OptionModule module;

    private List<String> logosList = new ArrayList<>();
    private List<OptionModule> agendasList = new ArrayList<>();
    private List<OptionModule> subjectsList = new ArrayList<>();

    public void setSelectedLogo(String l) {
        module.setLogo(l);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        Utils.setNightMode(this, true);
        setContentView(R.layout.activity_edit_options);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        logosList = Utils.generateLogosList(this);

        int id = -1;
        if (getIntent().getExtras() != null)
            id = getIntent().getExtras().getInt("id");

        if (id != -1)
            module = MemorisiaDatabase.getInstance(context).optionModuleDao().getOptionModuleOfId(id);
        else
            module = ModulesUtils.createOptionModuleFromBundle(getIntent().getExtras());

        getComponents();
        generateAgendaList();
        generateSubjectList();
        createSpinners();
        setDefaultComponentsValues();


        agendaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                int selected = subjectSpinner.getSelectedItemPosition();
                updateSubjectSpinner();
                if (selected < subjectsList.size())
                    subjectSpinner.setSelection(selected);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
    }

    private void setDefaultComponentsValues() {
        setupSwitch(agendaSwitch, agendaSpinner);
        setupSwitch(subjectSwitch, subjectSpinner);
        colorButton.setBackgroundColor(Color.parseColor(module.getColor()));
        if (module.getId() != -1) {
            title.setText(module.getText());
            setTitle(getResources().getString(R.string.editing) + " " + module.getText());
            if (logosList.size() > 0)
                logosGridView.setAdapter(new LogosListAdapter(this, logosList, module.getLogo(), module.getColor()));
            setupSpinners();


        } else {
            switch (module.getType()) {
                case OptionModule.SUBJECT:
                    setTitle(R.string.create_subject);
                    break;
                case OptionModule.WORK_TYPE:
                    setTitle(R.string.create_work);
                    break;
                case OptionModule.AGENDA:
                    setTitle(R.string.create_agenda);
                    break;
            }
            title.setText(R.string.name);
            deleteButton.setVisibility(Button.GONE);
            deleteButton.setEnabled(false);
            if (logosList.size() > 0) {
                logosGridView.setAdapter(new LogosListAdapter(this, logosList, logosList.get(0), module.getColor()));
                module.setLogo(logosList.get(0));
            }
            setSpinnerState(agendaSpinner, false);
            setSpinnerState(subjectSpinner, false);
            agendaSwitch.setChecked(false);
            subjectSwitch.setChecked(false);
        }
        setParentSelectionVisibility(module.getType());
    }

    private void setParentSelectionVisibility(int type) {
        int viewState = View.GONE;
        if (type == OptionModule.AGENDA) {
            subjectSwitch.setVisibility(viewState);
            subjectSpinner.setVisibility(viewState);
            agendaSwitch.setVisibility(viewState);
            agendaSpinner.setVisibility(viewState);
        } else if (type == OptionModule.SUBJECT) {
            subjectSwitch.setVisibility(viewState);
            subjectSpinner.setVisibility(viewState);
        }
    }

    private void setupSpinners() {
        if (module.getParentId() != -1) {
            MemorisiaDatabase data = MemorisiaDatabase.getInstance(this);
            OptionModule parentModule = data.optionModuleDao().getOptionModuleOfId(module.getParentId());
            if (parentModule.getType() == OptionModule.AGENDA) {
                agendaSpinner.setSelection(ModulesUtils.getPosInList(agendasList, module.getParentId()));
                agendaSwitch.setChecked(true);
                setSpinnerState(agendaSpinner, true);
                subjectSwitch.setChecked(false);
                setSpinnerState(subjectSpinner, false);
            } else {
                if (parentModule.getParentId() == -1) {
                    agendaSwitch.setChecked(false);
                    setSpinnerState(agendaSpinner, false);
                } else {
                    agendaSpinner.setSelection(ModulesUtils.getPosInList(agendasList, parentModule.getParentId()));
                    agendaSwitch.setChecked(true);
                }
                subjectSwitch.setChecked(true);
                subjectSpinner.setSelection(ModulesUtils.getPosInList(subjectsList, module.getParentId()));
                setSpinnerState(subjectSpinner, true);
            }

        } else {
            setSpinnerState(agendaSpinner, false);
            setSpinnerState(subjectSpinner, false);
            agendaSwitch.setChecked(false);
            subjectSwitch.setChecked(false);
        }
    }

    private void setSpinnerState(Spinner spinner, boolean state) {
        if (state)
            spinner.setAlpha(1f);
        else
            spinner.setAlpha(0.5f);
        spinner.setEnabled(state);
    }

    private void setupSwitch(Switch s, Spinner spin) {
        final Spinner spinner = spin;
        s.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    spinner.setEnabled(true);
                    spinner.setAlpha(1f);
                    updateSubjectSpinner();
                } else {
                    spinner.setEnabled(false);
                    spinner.setAlpha(0.5f);
                    updateSubjectSpinner();
                }
            }
        });
    }

    private void updateSubjectSpinner() {
        generateSubjectList();
        createSubjectSpinner();
    }

    private void generateModule() {
        module.setText(title.getText().toString());
        if (!agendaSwitch.isChecked() && !subjectSwitch.isChecked())
            module.setParentId(-1);
        else if (agendaSwitch.isChecked() && !subjectSwitch.isChecked())
            module.setParentId(agendasList.get(agendaSpinner.getSelectedItemPosition()).getId());
        else if (subjectSwitch.isChecked())
            module.setParentId(subjectsList.get(subjectSpinner.getSelectedItemPosition()).getId());
        // Logo and color are set on click
        // Change subject and agenda from works if parent changed
        if (module.getType() == OptionModule.SUBJECT) {
            changeWorkAgendaFromSubject(module.getId(), module.getParentId());
        } else if (module.getType() == OptionModule.WORK_TYPE) {
            if (module.getParentId() != -1) {
                MemorisiaDatabase db = MemorisiaDatabase.getInstance(this);
                OptionModule parent = db.optionModuleDao().getOptionModuleOfId(module.getParentId());
                if (parent.getType() == OptionModule.AGENDA)
                    changeWorkAgendaFromType(module.getId(), module.getParentId());
                else
                    changeWorkSubjectFromType(module.getId(), module.getParentId());
            }
        }
    }

    private void changeWorkAgendaFromSubject(int subjectId, int agendaId) {
        if (agendaId != -1) {
            MemorisiaDatabase db = MemorisiaDatabase.getInstance(this);
            List<WorkModule> workList = db.workModuleDao().getWorkModulesOfSubject(subjectId);
            for (WorkModule work : workList) {
                work.setAgendaId(agendaId);
                db.workModuleDao().insertWorkModules(work);
            }
        }
    }

    private void changeWorkSubjectFromType(int workId, int subjectId) {
        if (subjectId != -1) {
            MemorisiaDatabase db = MemorisiaDatabase.getInstance(this);
            List<WorkModule> workList = db.workModuleDao().getWorkModulesOfWorkType(workId);
            for (WorkModule work : workList) {
                work.setSubjectId(subjectId);
                db.workModuleDao().insertWorkModules(work);
            }
            OptionModule subject = db.optionModuleDao().getOptionModuleOfId(subjectId);
            if (subject.getParentId() != -1) {
                changeWorkAgendaFromType(workId, subject.getParentId());
            }
        }
    }

    private void changeWorkAgendaFromType(int workId, int agendaId) {
        if (agendaId != -1) {
            MemorisiaDatabase db = MemorisiaDatabase.getInstance(this);
            List<WorkModule> workList = db.workModuleDao().getWorkModulesOfWorkType(workId);
            for (WorkModule work : workList) {
                work.setAgendaId(agendaId);
                db.workModuleDao().insertWorkModules(work);
            }
        }
    }

    /**
     * Gets graphical components from the activity
     */
    private void getComponents() {
        title = findViewById(R.id.moduleName);
        logosGridView = findViewById(R.id.logosContainer);
        colorButton = findViewById(R.id.moduleColor);
        deleteButton = findViewById(R.id.deleteModule);
        agendaSwitch = findViewById(R.id.agendaSwitch);
        agendaSpinner = findViewById(R.id.agendaSpinner);
        subjectSwitch = findViewById(R.id.subjectSwitch);
        subjectSpinner = findViewById(R.id.subjectSpinner);
    }

    private void generateSubjectList() {
        MemorisiaDatabase data = MemorisiaDatabase.getInstance(this);
        if (agendaSwitch.isChecked())
            subjectsList = ModulesUtils.sortOptionModuleListByName(data.optionModuleDao().getOptionModulesOfParentId(OptionModule.SUBJECT, getSelectedParentId(OptionModule.AGENDA)), false);
        else
            subjectsList = ModulesUtils.sortOptionModuleListByName(data.optionModuleDao().getOptionModulesOfParentId(OptionModule.SUBJECT, -1), false);
    }

    private void generateAgendaList() {
        MemorisiaDatabase data = MemorisiaDatabase.getInstance(this);
        agendasList = ModulesUtils.sortOptionModuleListByName(data.optionModuleDao().getOptionModulesOfType(OptionModule.AGENDA), false);
    }

    private int getSelectedParentId(int type) {
        int id = -1;
        if (type == OptionModule.AGENDA)
            id = agendasList.get(agendaSpinner.getSelectedItemPosition()).getId();
        else if (type == OptionModule.SUBJECT)
            id = subjectsList.get(subjectSpinner.getSelectedItemPosition()).getId();
        return id;
    }


    /**
     * Creates the spinners based on the available modules
     */
    private void createSpinners() {
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(this, R.layout.simple_icon_spinner, agendasList);
        agendaSpinner.setAdapter(adapter);
        createSubjectSpinner();
    }

    private void createSubjectSpinner() {
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(this, R.layout.simple_icon_spinner, subjectsList);
        subjectSpinner.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Shows a color picker dialog to the user, allowing him to choose a color for the module
     *
     * @param v View that called the method
     */
    public void onClickColorPicker(View v) {
        AmbilWarnaDialog dialog = new AmbilWarnaDialog(this, Color.parseColor(module.getColor()), new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onOk(AmbilWarnaDialog dialog, int intColor) {
                colorButton.setBackgroundColor(intColor);
                module.setColor(String.format("#%06X", (0xFFFFFF & intColor)));
                if (logosList.size() > 0)
                    logosGridView.setAdapter(new LogosListAdapter(context, logosList, module.getLogo(), module.getColor()));
            }

            @Override
            public void onCancel(AmbilWarnaDialog dialog) {
                // cancel was selected by the user
            }

        });
        dialog.show();
    }

    /**
     * Saves the module corresponding to user selection and exits the activity
     *
     * @param v View that called the method
     */
    public void onClickDone(View v) {
        generateModule();
        long id = MemorisiaDatabase.getInstance(context).optionModuleDao().insertOptionModules(module)[0];
        // Add to selected agendas list if module is agenda
        if (module.getType() == OptionModule.AGENDA) {
            List<Integer> selectedAgendas = Utils.getSelectedAgendasFromPrefs(context);
            selectedAgendas.add((int) id);
            Utils.saveSelectedAgendasToPrefs(context, selectedAgendas);
        }
        exitEditOptions();
    }

    /**
     * Shows a confirm dialog when the user clicks the delete button
     *
     * @param v View that called the method
     */
    public void onClickDelete(View v) {
        showConfirmDeleteDialog();
    }

    /**
     * Shows a confirm delete dialog
     */
    private void showConfirmDeleteDialog() {
        ContextThemeWrapper ctw = new ContextThemeWrapper(this, R.style.AppTheme);
        final AlertDialog.Builder builder = new AlertDialog.Builder(ctw);
        builder.setMessage(getResources().getString(R.string.warning_delete_non_empty));
        builder.setCancelable(true);
        builder.setPositiveButton(
                getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i) {
                        MemorisiaDatabase db = MemorisiaDatabase.getInstance(context);
                        if (db.optionModuleDao().getOptionModulesOfType(module.getType()).size() == 1)
                            showErrorDialog(getResources().getString(R.string.cannot_delete_last));
                        else {
                            // db.optionModuleDao().deleteOptionModules(module);
                            deleteModulesFromDatabase();
                            exitEditOptions();
                        }
                        dialog.cancel();
                    }
                });

        builder.setNegativeButton(
                getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    private void deleteModulesFromDatabase() {
        List<Integer> workIdList = new ArrayList<>();
        List<Integer> subjectIdList = new ArrayList<>();
        List<Integer> workTypeIdList = new ArrayList<>();
        MemorisiaDatabase db = MemorisiaDatabase.getInstance(context);
        Log.w("Work", "Deleting !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        switch (module.getType()) {
            case OptionModule.AGENDA: // get all works, subject and work types associated with this agenda
                for (WorkModule work : db.workModuleDao().getWorkModulesOfAgenda(module.getId())){
                    workIdList.add(work.getId());
                    //Log.w("Work", "Text : " + work.getText());
                }
                for (OptionModule subject : db.optionModuleDao().getOptionModulesOfParentId(OptionModule.SUBJECT, module.getId())){
                    subjectIdList.add(subject.getId());
                    //Log.w("Subject", subject.getText());
                    for (OptionModule workType : db.optionModuleDao().getOptionModulesOfParentId(OptionModule.WORK_TYPE, subject.getId())){
                        workTypeIdList.add(workType.getId());
                        //Log.w("Work Type", workType.getText());
                    }
                }
                for (OptionModule workType : db.optionModuleDao().getOptionModulesOfParentId(OptionModule.WORK_TYPE, module.getId())){
                    workTypeIdList.add(workType.getId());
//                    Log.w("Work Type", workType.getText());
                }
                break;
            case OptionModule.SUBJECT: // get all works and work types associated with this subject
                for (WorkModule work : db.workModuleDao().getWorkModulesOfSubject(module.getId())){
                    workIdList.add(work.getId());
//                    Log.w("Work", "Text : " + work.getText());
                }
                for (OptionModule workType : db.optionModuleDao().getOptionModulesOfParentId(OptionModule.WORK_TYPE, module.getId())){
                    workTypeIdList.add(workType.getId());
//                    Log.w("Work Type", workType.getText());
                }
                break;
            case OptionModule.WORK_TYPE: // get all works associated with this work type
                for (WorkModule work : db.workModuleDao().getWorkModulesOfWorkType(module.getId())){
                    workIdList.add(work.getId());
//                    Log.w("Work", "Text : " + work.getText());
                }
                break;
        }

        if (workIdList.size() > 0)
            db.workModuleDao().deleteWorkModulesOfIds(workIdList);
        if (workTypeIdList.size() > 0)
            db.optionModuleDao().deleteOptionModulesOfIds(workTypeIdList);
        if (subjectIdList.size() > 0)
            db.optionModuleDao().deleteOptionModulesOfIds(subjectIdList);
        db.optionModuleDao().deleteOptionModules(module);
    }

    /**
     * Shows an error dialog telling the user he cannot delete an entry
     *
     * @param message Message to display to the user
     */
    private void showErrorDialog(String message) {
        ContextThemeWrapper ctw = new ContextThemeWrapper(this, R.style.AppTheme);
        final AlertDialog.Builder builder = new AlertDialog.Builder(ctw);
        builder.setMessage(message);
        builder.setCancelable(true);
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Exits the activity without adding it to the backtrace
     */
    private void exitEditOptions() {
        Intent intent = new Intent(this, OptionsListActivity.class);
        intent.setData(Uri.parse(module.getType() + ""));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}
