/*
 * Copyright (c) 2018.
 * This file is part of Memorisia.
 *
 * Memorisia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Memorisia is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Memorisia.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.clubinfo.insat.memorisia.fragments;


import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clubinfo.insat.memorisia.activities.MainActivity;
import com.clubinfo.insat.memorisia.adapters.WorksRecyclerAdapter;
import com.clubinfo.insat.memorisia.database.MemorisiaDatabase;
import com.clubinfo.insat.memorisia.modules.OptionModule;
import com.clubinfo.insat.memorisia.modules.WorkModule;
import com.clubinfo.insat.memorisia.utils.ModulesUtils;

import java.util.List;

public class WorkViewFragment extends BaseFragment {

    private int parentId;
    private int parentType;

    public int getParentId() {
        return parentId;
    }

    public int getParentType() {
        return parentType;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflateLayout(inflater, container, true);

        parentId = getArguments().getInt("id");
        parentType = getArguments().getInt("type");
        getActivity().setTitle(getArguments().getString("text"));
        generateList();
        registerForContextMenu(getRecyclerView());
        return v;
    }

    /**
     * Generates the works list based on the selected sort type
     */
    @Override
    public void generateList() {
        MemorisiaDatabase db = MemorisiaDatabase.getInstance(getActivity());
        MainActivity act = (MainActivity) getActivity();
        boolean isParentSubject = parentType == OptionModule.SUBJECT;
        List<WorkModule> workList;
        if (isParentSubject)
            workList = db.workModuleDao().getWorkModulesOfSubject(act.getSelectedAgendas(), parentId);
        else
            workList = db.workModuleDao().getWorkModulesOfWorkType(act.getSelectedAgendas(), parentId);
        RecyclerView.Adapter mAdapter;
        switch (getCurrentSortType()) {
            case SORT_1:
                mAdapter = new WorksRecyclerAdapter(getActivity(), ModulesUtils.sortWorkModuleListByName(workList, isReverseSort()));
                break;
            case SORT_2:
                mAdapter = new WorksRecyclerAdapter(getActivity(), ModulesUtils.sortWorkModuleListByPriority(workList, isReverseSort()));
                break;
            case SORT_3:
                mAdapter = new WorksRecyclerAdapter(getActivity(), ModulesUtils.sortWorkModuleListByWorkType(workList, getActivity(), isReverseSort()));
                break;
            case SORT_4:
                mAdapter = new WorksRecyclerAdapter(getActivity(), ModulesUtils.sortWorkModuleListByDate(workList, isReverseSort()));
                break;
            default:
                mAdapter = new WorksRecyclerAdapter(getActivity(), workList);
                break;
        }
        getRecyclerView().setAdapter(mAdapter);
    }

}
