# Memorisia

### Memorise your things!

This is a project made in my spare time to learn android app development, so do not expect anything crazy.

## Description

This application allows you to save works, associated to a work type, subject and agenda, to keep all your tasks organized.

For each work, you can set a due date, a priority and a description.

And that's about it, hope you will find it useful !

## Installation
App is available in beta on the [PlayStore here](https://play.google.com/store/apps/details?id=com.clubinfo.insat.memorisia).

## Contribution
Feel free to contribute to the project, either by reporting bugs, helping with the code or simply with translations.

## Translations
App is currently available in English and French.

## More
It should be compatible with android 4..0.3+ (API 15+).
While it is supposed to be compatible with early Android 4 versions, it may have some problems compared to newer versions of Android.
If you find a bug, please open a new Issue [here](https://github.com/Keplyx/Memorisia/issues) on GitHub.


Application licensed under GPLv3.
